# Transcended Paperbin

Paperbin game engine, trying this again! Logical successor to the TeaParty.

Uses OpenGL 4.5, GLEW, and SDL2.

## STYLE GUIDE
 - `assert` should be used sparingly, use Paperbin's exception system as much as you can when it is implemented.