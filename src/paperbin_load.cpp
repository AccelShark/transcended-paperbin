//
// Created by acceleration on 8/23/19.
//

#include "paperbin_load.hpp"

using namespace paperbin;

void load_shader::compile() {
    con_loop::printc(con_level::debug, "load_shader::compile");
    if (!_compiled) {
        con_loop::printc(con_level::info, "load_shader::compile - compiling");
        glCompileShader(_idx);
        GLint status;
        glGetShaderiv(_idx, GL_COMPILE_STATUS, &status);
        if (status != GL_TRUE) {
            con_loop::printc(con_level::error, "load_shader::compile - compilation failed");
            char buffer[4096]{0};
            glGetShaderInfoLog(_idx, 4096, nullptr, buffer);
            con_loop::printc(con_level::error, buffer);
        }
        _compiled = status == GL_TRUE;
    }
    con_loop::printc(con_level::trace, "load_shader::compile - leaving");
}

std::unique_ptr<load_program> load_program::link(std::forward_list<load_shader> &&shaders) {
    con_loop::printc(con_level::debug, "load_program::link");
    auto program = std::make_unique<load_program>(load_program{glCreateProgram(), shaders});
    for (auto &&i : program->shaders) {
        con_loop::printc(con_level::trace, "load_program::link - attaching a shader");
        glAttachShader(program->idx, i.get_id());
    }
    glBindFragDataLocation(program->idx, 0, "outColor");


    con_loop::printc(con_level::trace, "load_program::link - linking, will use");

    glLinkProgram(program->idx);

    GLint status;
    glGetProgramiv(program->idx, GL_LINK_STATUS, &status);
    if (status != GL_TRUE) {
        con_loop::printc(con_level::error, "load_program::link - link failed");
        char buffer[4096]{0};
        glGetProgramInfoLog(program->idx, 4096, nullptr, buffer);
        con_loop::printc(con_level::error, buffer);
    }

    glUseProgram(program->idx);

    con_loop::printc(con_level::trace, "load_program::link - leaving");
    return program;
}

GLuint load_shader::get_id() {
    con_loop::printc(con_level::debug, "load_shader::get_id");
    return _idx;
}

load_shader::load_shader(const char *path, const char *name, shader_type type) : load_base(path, name), _type(type),
                                                                                 _idx(glCreateShader(
                                                                                         static_cast<GLuint>(type))) {
    con_loop::printc(con_level::debug, "load_shader::load_shader");
    _text = strdup(_conts.data());
    glShaderSource(_idx, 1, &_text, nullptr);
    con_loop::printc(con_level::trace, "load_shader::load_shader - leaving");
}

load_shader::~load_shader() {
    con_loop::printc(con_level::debug, "load_shader::~load_shader");
    glDeleteShader(_idx);
    std::free(_text);
    con_loop::printc(con_level::trace, "load_shader::~load_shader - leaving");
}

shader_type load_shader::get_type() {
    con_loop::printc(con_level::debug, "load_shader::get_type");
    return _type;
}

load_base::load_base(const char *path, const char *name) {
    con_loop::printc(con_level::debug, "load_base::load_base");
    _path = new char[512]{0};
    _size = std::snprintf(_path, 511, "%s/%s", path, name);
    auto file = std::fopen(_path, "r");

    con_loop::printc(con_level::trace, "load_base::load_base - checking assertions, then loading file");
    assert(file);
    int c;
    while ((c = std::fgetc(file)) != EOF) {
        _conts.emplace_back(c);
    }
    assert(std::feof(file));
    con_loop::printc(con_level::trace, "load_base::load_base - successful, leaving");
}

load_base::~load_base() {
    con_loop::printc(con_level::debug, "load_base::~load_base");
    delete[] _path;
}

load_program::~load_program() {
    con_loop::printc(con_level::debug, "load_program::~load_program");
    for (auto &&i : shaders) {
        glDetachShader(idx, i.get_id());
    }
    glDeleteProgram(idx);
    con_loop::printc(con_level::trace, "load_program::~load_program - leaving");
}

