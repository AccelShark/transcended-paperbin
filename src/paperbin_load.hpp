//
// Created by acceleration on 8/23/19.
//

#ifndef PAPERBIN_PAPERBIN_LOAD_HPP
#define PAPERBIN_PAPERBIN_LOAD_HPP

#include "paperbin.hpp"
#include "paperbin_con.hpp"

namespace paperbin {
    class load_base {
        char *_path;
        std::size_t _size;
    protected:
        std::vector<char> _conts{};
    public:
        load_base(const char *, const char *);

        ~load_base();
    };

    enum class shader_type : GLuint {
        fragment = GL_FRAGMENT_SHADER,
        vertex = GL_VERTEX_SHADER,
        geometry = GL_GEOMETRY_SHADER,
        compute = GL_COMPUTE_SHADER,
    };

    class load_shader;

    struct load_program {
        GLuint idx;
        std::forward_list<load_shader> shaders;

        static std::unique_ptr<load_program> link(std::forward_list<load_shader> &&);

        ~load_program();
    };

    class load_shader : public load_base {
        GLuint _idx;
        shader_type _type;
        char *_text;
        bool _compiled = false;
    public:
        load_shader(const char *, const char *, shader_type);

        void compile();

        shader_type get_type();

        GLuint get_id();

        ~load_shader();
    };
}


#endif //PAPERBIN_PAPERBIN_LOAD_HPP
