//
// Created by acceleration on 8/21/19.
//

#include "paperbin_sim.hpp"

using namespace paperbin;

bool sim_loop::call(bool keep) {
    con_loop::printc(con_level::debug, "sim_loop::call");
    SDL_Event windowEvent;
    if (SDL_PollEvent(&windowEvent)) {
        if (windowEvent.type == SDL_QUIT ||
            (windowEvent.type == SDL_KEYUP && windowEvent.key.keysym.sym == SDLK_ESCAPE)) {
            con_loop::printc(con_level::info, "sim_loop::call - will terminate");
            return (*keep_alive = false);
        }
    }

    SDL_GL_SwapWindow(window);
    con_loop::printc(con_level::trace, "sim_loop::call - leaving");

    return *keep_alive;
}

sim_loop::sim_loop(std::uint_fast16_t width, std::uint_fast16_t height, bool fullscreen, bool &alive_bool) : keep_alive(
        &alive_bool) {
    con_loop::printc(con_level::debug, "sim_loop::sim_loop");

    SDL_Init(SDL_INIT_EVERYTHING);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 5);
    SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
    window = SDL_CreateWindow("Transcended Paperbin", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height,
                              SDL_WINDOW_OPENGL | (fullscreen ? SDL_WINDOW_FULLSCREEN : 0));
    ctx = SDL_GL_CreateContext(window);

    con_loop::printc(con_level::trace, "sim_loop::sim_loop - leaving");
}

sim_loop::~sim_loop() {
    con_loop::printc(con_level::debug, "sim_loop::~sim_loop");

    SDL_GL_DeleteContext(ctx);
    SDL_DestroyWindow(window);
    SDL_Quit();

    con_loop::printc(con_level::trace, "sim_loop::~sim_loop - leaving");
}
