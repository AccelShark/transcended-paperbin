//
// Created by acceleration on 8/20/19.
//

#include "paperbin_con.hpp"

using namespace paperbin;

// TODO: Scripting instances

// Values that are set for each con_loop
con_level minimum_debug;
bool initialized = false;
con_job jobs = 0;
std::unique_ptr<std::queue<con_jfun>> functions;

void con_loop::init(con_level init_minimum_debug) {
    assert(!initialized);

    minimum_debug = init_minimum_debug;
    functions = std::make_unique<std::queue<con_jfun>>();
    initialized = true;
    printc(con_level::debug, "con_loop::init");
}

void con_loop::deinit() {
    assert(initialized);

    functions = nullptr;
    printc(con_level::debug, "con_loop::deinit");
    initialized = false;
}

void con_loop::printc(con_level level, const char *string) {
    assert(initialized);

    if (static_cast<std::uint_fast8_t>(level) >= static_cast<std::uint_fast8_t>(minimum_debug)) {
        auto time = std::chrono::system_clock::now();
        auto c_representable_time = std::chrono::system_clock::to_time_t(time);
        auto time_msecs = std::chrono::duration_cast<std::chrono::milliseconds>(time.time_since_epoch());

        char representable_time[32]{0};
        std::strftime(representable_time, sizeof(representable_time), "%T", std::localtime(&c_representable_time));
        switch (level) {
            case con_level::trace:
                std::fprintf(stderr, "(%s.%.3lu) [TRACE] %s\n", representable_time, time_msecs.count() % 1000, string);
                break;
            case con_level::debug:
                std::fprintf(stderr, "(%s.%.3lu) [DEBUG] %s\n", representable_time, time_msecs.count() % 1000, string);
                break;
            case con_level::info:
                std::fprintf(stderr, "(%s.%.3lu) [INFO] %s\n", representable_time, time_msecs.count() % 1000, string);
                break;
            case con_level::warning:
                std::fprintf(stderr, "(%s.%.3lu) [WARNING] %s\n", representable_time, time_msecs.count() % 1000,
                             string);
                break;
            case con_level::error:
                std::fprintf(stderr, "(%s.%.3lu) [ERROR] %s\n", representable_time, time_msecs.count() % 1000, string);
                break;
            case con_level::critical:
                std::fprintf(stderr, "(%s.%.3lu) [CRITICAL] %s\n", representable_time, time_msecs.count() % 1000,
                             string);
                break;
        }
    }
}

void con_loop::tick(bool keep_alive) {
    assert(initialized);

    printc(con_level::debug, "con_loop::tick");
    auto jpending = job_num();
    while (jpending--) {
        job_call(keep_alive);
    }
    printc(con_level::trace, "con_loop::tick - leaving");
}

con_job con_loop::job_push(con_jfun &&jfun) {
    assert(initialized);
    printc(con_level::debug, "con_loop::job_push");

    functions->push(std::move(jfun));

    printc(con_level::trace, "con_loop::job_push - leaving");
    return ++jobs;
}

con_job con_loop::job_pop() {
    assert(initialized);
    printc(con_level::debug, "con_loop::job_pop");

    functions->pop();

    printc(con_level::trace, "con_loop::job_pop - leaving");
    return --jobs;
}

con_job con_loop::job_num() {
    assert(initialized);
    printc(con_level::debug, "con_loop::job_num");
    return jobs;
}

void con_loop::job_call(bool keep_alive) {
    assert(job_num() > 0);
    printc(con_level::debug, "con_loop::job_call");

    con_jfun jhere = nullptr;
    std::swap(jhere, functions->front());
    functions->pop();

    if (jhere->call(keep_alive))
        functions->push(std::move(jhere));
    else
        printc(con_level::trace, "con_loop::job_call - terminating frontmost jfun");

    printc(con_level::trace, "con_loop::job_call - leaving");
}

con_object::~con_object() {
    con_loop::printc(con_level::debug, "con_object::~con_object");
}
