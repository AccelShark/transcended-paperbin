//
// Created by acceleration on 8/20/19.
//

#ifndef PAPERBIN_PAPERBIN_CON_HPP
#define PAPERBIN_PAPERBIN_CON_HPP

#include "paperbin.hpp"

/** Define Paperbin namespace */
namespace paperbin {
    /** A base worker; belongs in a `con_loop`, can be killed. */
    struct con_object {
        ~con_object();

        /** Perform the con_object's task.
         *  @return false: should kill this process.
         *          true:  keep this process alive.
         */
        virtual bool call(bool) = 0;
    };

    using con_job = std::uint_fast32_t;
    using con_jfun = std::unique_ptr<con_object>;
    enum class con_level : std::uint_fast8_t {
        trace = 0x01,
        debug = 0x02,
        info = 0x10,
        warning = 0x20,
        error = 0x40,
        critical = 0x80,
    };
    namespace con_loop {
        void init(con_level);

        void printc(con_level, const char *);

        void tick(bool);

        void job_call(bool);

        con_job job_push(con_jfun &&);

        con_job job_num();

        con_job job_pop();

        void deinit();
    }
}


#endif //PAPERBIN_PAPERBIN_CON_HPP
