//
// Created by acceleration on 8/17/19.
//

#include "paperbin.hpp"
#include "paperbin_con.hpp"
#include "paperbin_conlib.hpp"
#include "paperbin_sim.hpp"

extern "C" {
char *optarg;
int optind, optopt;
}

volatile std::sig_atomic_t keep_alive = 1;

extern "C" void sigint_handler(int signal) {
    keep_alive = 0;
}

int main(int argc, char *const argv[]) {
    auto res_width = std::uint_fast16_t(1024);
    auto res_height = std::uint_fast16_t(600);
    int c;
    auto debug_level = paperbin::con_level::info;
    char *base_dir;
    bool fullscreen = false;
    bool headless = false;
    while ((c = getopt(argc, argv, "hfd:s:b:"))) {
        switch (c) {
            case 'h': {
                headless = true;
                break;
            }
            case 'd': {
                debug_level = static_cast<paperbin::con_level>(atoi(optarg));
                break;
            }
            case 'f': {
                fullscreen = true;
                break;
            }
            case 's': {
                // set size of screen
                auto match_res = std::regex{R"(([0-9]+)[x\ ]([0-9]+))"};
                std::cmatch matches;
                std::regex_match(optarg, matches, match_res);
                if (matches.size() == 3) {
                    res_width = atoi(matches[1].str().c_str());
                    res_height = atoi(matches[2].str().c_str());
                }
                break;
            }
            case 'b': {
                base_dir = strdup(optarg);
            }
            default: {
                bool keepalive_window = true;
                paperbin::con_loop::init(debug_level);
                paperbin::con_loop::printc(paperbin::con_level::info, "paperbin initialized");

                if (headless)
                    std::signal(SIGINT, sigint_handler);
                else
                    paperbin::con_loop::job_push(
                            std::make_unique<paperbin::sim_loop>(res_width, res_height, fullscreen, keepalive_window));


                // paperbin::con_loop::job_push(std::make_unique<paperbin::conlib_goat>());

                do {
                    paperbin::con_loop::tick(keep_alive == 1);
                    std::this_thread::sleep_for(std::chrono::milliseconds(33));
                } while ((keep_alive == 1) && keepalive_window);

                paperbin::con_loop::printc(paperbin::con_level::info, "paperbin quitting gracefully");
                paperbin::con_loop::deinit();
                return EXIT_SUCCESS;
            }
        }
    }
    return EXIT_FAILURE;
}