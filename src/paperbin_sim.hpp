//
// Created by acceleration on 8/21/19.
//

#ifndef PAPERBIN_PAPERBIN_SIM_HPP
#define PAPERBIN_PAPERBIN_SIM_HPP

#include "paperbin.hpp"
#include "paperbin_con.hpp"

namespace paperbin {
    struct sim_loop : con_object {
        SDL_Window *window{nullptr};
        SDL_GLContext ctx;
        std::unique_ptr<bool> keep_alive{nullptr};

        sim_loop(std::uint_fast16_t, std::uint_fast16_t, bool, bool &);

        ~sim_loop();

        bool call(bool) override;
    };
}


#endif //PAPERBIN_PAPERBIN_SIM_HPP
