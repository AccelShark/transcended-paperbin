//
// Created by acceleration on 8/17/19.
//

#ifndef PAPERBIN_PAPERBIN_HPP
#define PAPERBIN_PAPERBIN_HPP

#define GLEW_STATIC

// C Includes
#include <cassert>
#include <csignal>
#include <cstdint>
#include <cstdio>
#include <ctime>

// C++ Includes
#include <chrono>
#include <forward_list>
#include <memory>
#include <regex>
#include <thread>
#include <queue>

// POSIX Includes
#include <unistd.h>
// GLEW
#include <GL/glew.h>
// SDL
#include <SDL2/SDL.h>


#endif //PAPERBIN_PAPERBIN_HPP
