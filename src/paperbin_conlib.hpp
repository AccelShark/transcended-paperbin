//
// Created by acceleration on 8/21/19.
//

#ifndef PAPERBIN_PAPERBIN_CONLIB_HPP
#define PAPERBIN_PAPERBIN_CONLIB_HPP

#include "paperbin.hpp"
#include "paperbin_con.hpp"

namespace paperbin {
    struct conlib_goat : con_object {
        bool call(bool) override;
    };
}


#endif //PAPERBIN_PAPERBIN_CONLIB_HPP
