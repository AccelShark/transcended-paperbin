// This is for transforming attributes into vertices, literally XYZ.
// NOTE: This includes axis-angle conventions and not plain old Euler.
#version 450 core
layout(location = 1) in vec4 vert_axis;
layout(location = 2) in vec4 vert_angle;
layout(location = 3) in vec4 vert_paint;
layout(location = 11) in vec4 vert_modaxis;
layout(location = 12) in vec4 vert_modangle;
out vec4 frag_paint;
layout(location = 21) uniform vec4 cam_axis;
layout(location = 22) uniform vec4 cam_angle;
//layout(location = 3) uniform vec4 cam_pin;
layout(location = 23) uniform float cam_aspect;
layout(location = 24) uniform float cam_fov;
const float PI = 3.1415926535897932384626433832795;
// Identity Matrix
const mat4x4 ident = mat4x4(
1.0f, 0.0f, 0.0f, 0.0f,
0.0f, 1.0f, 0.0f, 0.0f,
0.0f, 0.0f, 1.0f, 0.0f,
0.0f, 0.0f, 0.0f, 1.0f);

const float near = 0.01f;
const float far = 100.0f;
const float cull_coeff = (far - near);

mat4x4 translate(vec4 vector) {
    return mat4x4(
    1.0f, 0.0f, 0.0f, 0.0f,
    0.0f, 1.0f, 0.0f, 0.0f,
    0.0f, 0.0f, 1.0f, 0.0f,
    vec4(vector.xyz, 1.0f));
}

mat4x4 scale_to_aspect() {
    return mat4x4(
    cam_aspect, 0.0f, 0.0f, 0.0f,
    0.0f, 1.0f, 0.0f, 0.0f,
    0.0f, 0.0f, 1.0f, 0.0f,
    0.0f, 0.0f, 0.0f, 1.0f);
}
// This uses Rodrigues' Rotation Formula to get a matrix for rotations.
mat4x4 rotate(vec4 angle) {
    vec4 vert_n = vec4(normalize(angle.xyz), 1.0f);
    float vert_rotate = mod(angle.w, PI * 2.0f);
    // Antisymmetric Matrix
    mat4x4 anti = mat4x4(
    0.0f, -vert_n.z, vert_n.y, 0.0f,
    vert_n.z, 0.0f, -vert_n.x, 0.0f,
    -vert_n.y, vert_n.x, 0.0f, 0.0f,
    0.0f, 0.0f, 0.0f, 1.0f);
    return ident + (sin(vert_rotate) * anti) + ((1.0f - cos(vert_rotate)) * (anti * anti));
}

mat4x4 view() {
    return mat4x4(
    1.0f/tan(cam_fov),
    0.0f,
    0.0f,
    0.0f,

    0.0f,
    cam_aspect,
    0.0f,
    0.0f,

    0.0f,
    0.0f,
    (far + near) / cull_coeff,
    1.0f,

    0.0f,
    0.0f,
    (-2.0f * far * near) / cull_coeff,
    0.0f);
}
void main() {
    mat4x4 v = view() * translate(cam_axis) * rotate(cam_angle) * scale_to_aspect();
    // v *= rotate(vec4(cam_angle.xyz, 1.0f));
    vec4 va = vert_axis + vert_modaxis;
    vec4 vert = v * va * rotate(vert_angle + vert_modangle);
    frag_paint = vec4(vert_paint.rgb, vert.w / 8.0f);// Pass d's z val for Z feedback, thus allowing us to discard.
    gl_Position = vert;
}
